defmodule Graditar.Mixfile do
  use Mix.Project

  def project do
    [
      app: :graditar,
      version: "2.0.0",
      elixir: "~> 1.4",
      build_embedded: Mix.env == :prod,
      start_permanent: Mix.env == :prod,
      deps: deps(),
      description: description(),
      package: package(),
    ]
  end

  def application do
    [applications: [:logger]]
  end

  defp description do
    """
    A set of helper modules to generate gradient-based avatars.
    """
  end

  defp package do
    [
      name: :graditar,
      files: ["lib", "mix.exs", "README.md", "LICENSE"],
      licenses: ["ISC"],
      maintainers: ["Sean-Patrick Ortencio Clayton"],
      links: %{
        "GitLab" => "https://gitlab.com/seanclayton/graditar"
      },
    ]
  end

  defp deps do
    [
      {:xml_builder, "~> 0.0.8"},
      {:mix_test_watch, "~> 0.2", only: :dev},
      {:color_utils, "~> 0.2.0"},
      {:ex_doc, ">= 0.0.0", only: :dev},
    ]
  end
end
