# Graditar

A set of helper modules to generate gradient-based avatars.

## Installation

1. Add `graditar` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [{:graditar, "~> 2.0.0"}]
end
```

2. Ensure `graditar` is started before your application:

```elixir
def application do
  [applications: [:graditar]]
end
```

## Examples

Generate a generic svg

```elixir
iex> Graditar.generate
"<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n<svg height=\"512px\" preserveAspectRatio=\"none\" viewBox=\"0 0 1 1\" width=\"512px\" xmlns=\"http://www.w3.org/2000/svg\">\n\t<linearGradient gradientTransform=\"rotate(315 .5 .5)\" gradientUnits=\"userSpaceOnUse\" id=\"gradient\" x1=\"0%\" x2=\"0%\" y1=\"0%\" y2=\"100%\">\n\t\t<stop offset=\"0%\" stop-color=\"rgb(212, 29, 140)\" stop-opacity=\"1\"/>\n\t\t<stop offset=\"100%\" stop-color=\"rgb(120, 211, 28)\" stop-opacity=\"1\"/>\n\t</linearGradient>\n\t<rect fill=\"url(#gradient)\" height=\"1\" width=\"1\" x=\"0\" y=\"0\"/>\n</svg>"
```

Generate from a string

```elixir
iex> Graditar.generate("bobthebuilder@canwefix.it")
"<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n<svg height=\"512px\" preserveAspectRatio=\"none\" viewBox=\"0 0 1 1\" width=\"512px\" xmlns=\"http://www.w3.org/2000/svg\">\n\t<linearGradient gradientTransform=\"rotate(225 .5 .5)\" gradientUnits=\"userSpaceOnUse\" id=\"gradient\" x1=\"0%\" x2=\"0%\" y1=\"0%\" y2=\"100%\">\n\t\t<stop offset=\"0%\" stop-color=\"rgb(145, 79, 1)\" stop-opacity=\"1\"/>\n\t\t<stop offset=\"100%\" stop-color=\"rgb(73, 145, 1)\" stop-opacity=\"1\"/>\n\t</linearGradient>\n\t<rect fill=\"url(#gradient)\" height=\"1\" width=\"1\" x=\"0\" y=\"0\"/>\n</svg>"
```

Add options

```elixir
iex> Graditar.generate("bobthebuilder@canwefix.it", %{ size: 128 })
"<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n<svg height=\"128px\" preserveAspectRatio=\"none\" viewBox=\"0 0 1 1\" width=\"128px\" xmlns=\"http://www.w3.org/2000/svg\">\n\t<linearGradient gradientTransform=\"rotate(225 .5 .5)\" gradientUnits=\"userSpaceOnUse\" id=\"gradient\" x1=\"0%\" x2=\"0%\" y1=\"0%\" y2=\"100%\">\n\t\t<stop offset=\"0%\" stop-color=\"rgb(145, 79, 1)\" stop-opacity=\"1\"/>\n\t\t<stop offset=\"100%\" stop-color=\"rgb(73, 145, 1)\" stop-opacity=\"1\"/>\n\t</linearGradient>\n\t<rect fill=\"url(#gradient)\" height=\"1\" width=\"1\" x=\"0\" y=\"0\"/>\n</svg>"
```
