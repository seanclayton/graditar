defmodule Graditar.Avatar do
  defstruct(
    hex: nil,
    primary_color: nil,
    secondary_color: nil,
    angle: nil,
  )
end